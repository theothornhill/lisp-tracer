(in-package #:lisp-tracer)

(defstruct color
  (red 0.0)
  (green 0.0)
  (blue 0.0))
